/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rahul
 */
public class Teacher {
    private String name;
    private String contact_no;
    private String email;
    private String password;
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    private String created_at = format.format(date);
    
    
    public Teacher(String name, String contact_no, String email, String password){
        this.name = name;
        this.contact_no = contact_no;
        this.email = email;
        this.password = password;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getContact(){
        return this.contact_no;
    }
     
    public String getEmail(){
        return this.email;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public String getCreatedDate(){
        return this.created_at;
    }
}
