/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

/**
 *
 * @author Giris
 */
// A Class that represents use-defined expception
public class QuestionsNotFoundException extends Exception
{
    public QuestionsNotFoundException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}
