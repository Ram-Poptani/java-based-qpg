/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import custom.Subject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Giris
 */
public class SubjectDB {
    private Connection conn;
    private PreparedStatement ps;
    
    
    public SubjectDB(){
        conn = MySQLConnect.getConnection();
        ps = null;
    }
    
//    public ResultSet getSubjectsWithTeachers() throws SQLException{
//        HashMap<String, String> subjectsWithTeachers;
//        subjectsWithTeachers = new HashMap<>();
//        
//        return rs;
//        while(rs.next()){
//            subjectsWithTeachers.put(rs.getString("subject_name"), rs.getString("teacher_name"));
//        }
//        return subjectsWithTeachers;
//    }    
    
    public void storeSubject(Subject s) throws SQLException{
        String sql = "INSERT INTO subjects(name, semester, created_at) values (?, ?, ?)";
        
        ps = conn.prepareStatement(sql);

        ps.setString(1, s.getName());
        ps.setInt(2, s.getSemester());
        ps.setString(3, s.getCreatedDate());
       
        ps.execute();
        
        this.storeSubjectInCourse(courseIdByCourseName(s.getCourse()), subjectIdBySubjectName(s.getName()));
    } 
    
    private int courseIdByCourseName(String course) throws SQLException{
        String sql = "SELECT id from courses WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, course);
        ResultSet rs = ps.executeQuery();
        int courseId = 0;
        if(rs.next()){
            courseId = rs.getInt("id");
        }
        return courseId;
    }
    
    private int subjectIdBySubjectName(String subject) throws SQLException{
        String sql = "SELECT id from subjects WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, subject);
        ResultSet rs = ps.executeQuery();
        int subjectId = 0;
        if(rs.next()){
            subjectId = rs.getInt("id");
        }
        return subjectId;
    }
    
    private void storeSubjectInCourse(int courseId, int subjectId) throws SQLException {
        String sql = "INSERT INTO course_subject(course_id, subject_id) values (?, ?)";
        
        ps = conn.prepareStatement(sql);

        ps.setInt(1, courseId);
        ps.setInt(2, subjectId);
        
        ps.execute();
    }
    
    public TableModel getSubjectsWithTeachers() throws SQLException {
        String sql = "SELECT subjects.name as Subject, teachers.name as Teacher from subjects INNER JOIN teachers ON subjects.user_id = teachers.id WHERE subjects.user_id IS NOT NULL";
        ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        ResultSetMetaData metaData;
        metaData = rs.getMetaData();
        int numberOfColumns = metaData.getColumnCount();
        Vector columnNames = new Vector();

        // Get the column names
        for (int column = 0; column < numberOfColumns; column++) {
            columnNames.addElement(metaData.getColumnLabel(column + 1));
        }

        // Get all rows.
        Vector rows = new Vector();

        while (rs.next()) {
            Vector newRow = new Vector();

            for (int i = 1; i <= numberOfColumns; i++) {
                newRow.addElement(rs.getObject(i));
            }

            rows.addElement(newRow);
        }

        return new DefaultTableModel(rows, columnNames);
    }
    
    public ArrayList<String> getNotAssignedSubjectsAsPerCourse(String course) throws SQLException {
        ArrayList<String> courses = new ArrayList<>();
        int courseId = this.courseIdByCourseName(course);
        String sql = "SELECT name From subjects JOIN course_subject ON subjects.id = course_subject.subject_id AND course_subject.course_id = ? WHERE subjects.user_id IS NULL";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, courseId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            courses.add(rs.getString("name"));
        }       
        return courses;
    }

    public void assignSubjectToTeacher(int subjectId, int teacherId) throws SQLException {
        String sql = "UPDATE subjects SET user_id = ? WHERE id = ?";
 
        ps = conn.prepareStatement(sql);

        ps.setInt(1, teacherId);
        ps.setInt(2, subjectId);
       
        ps.execute();
    }

    public void removeAssignedSubjectFromTeacher(int subjectId, int teacherId) throws SQLException {
        String sql = "UPDATE subjects SET user_id = NULL WHERE id = ? AND user_id = ?";
 
        ps = conn.prepareStatement(sql);

        ps.setInt(1, subjectId);
        ps.setInt(2, teacherId);
       
        ps.execute();
    }
}
