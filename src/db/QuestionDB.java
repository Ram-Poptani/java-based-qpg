/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import custom.Question;
import custom.QuestionsNotFoundException;
import generate.SemesterDB;
import generate.UnitTestDB;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rahul
 */
public class QuestionDB {
    private Connection conn;
    private PreparedStatement ps;
    UnitTestDB ub;
    SemesterDB sb;
    private final String UT = "UT";
    private final String SEMESTER = "SEMESTER";
    TeacherDB tb = new TeacherDB();
    
    public QuestionDB(){
        conn = MySQLConnect.getConnection();
        ps = null;
        ub = new UnitTestDB();
        sb = new SemesterDB();
    }

    public void generateUnitTestQuestionPaper(String subject, List chaptersList, String difficultyLevel) throws SQLException, QuestionsNotFoundException{
        ArrayList<Integer> chapterIDs = new ArrayList<>();
        chapterIDs = this.getChapterIDsForUnitTest(subject, chaptersList);
        this.generateQuestionPaper(this.UT, subject, chapterIDs, difficultyLevel);
    }
    
    public void generateSemesterQuestionPaper(String subject, String difficultyLevel) throws SQLException, QuestionsNotFoundException {
        ArrayList<Integer> chapterIDs = new ArrayList<>();
        chapterIDs = this.getChapterIDsForSemester(subject);
        this.generateQuestionPaper(this.SEMESTER, subject, chapterIDs, difficultyLevel);
    }
    
    private ArrayList<Integer> getChapterIDsForUnitTest(String subject, List chaptersList) throws SQLException{
        // TODO : 
        ArrayList<Integer> chapterIDs = new ArrayList<>();
        if(chaptersList.isEmpty()){
            chapterIDs = tb.getChapterIDsForSubject(subject);
        }else{
            chapterIDs = tb.getChapterIDsForChapters((ArrayList) chaptersList);
        }
        
        return chapterIDs;
    }
    
    private ArrayList<Integer> getChapterIDsForSemester(String subject) throws SQLException{
        ArrayList<Integer> chapterIDs = new ArrayList<>();
        chapterIDs = tb.getChapterIDsForSubject(subject);
        return chapterIDs;
    }
    
    
    public void generateQuestionPaper(String paperType, String subject, ArrayList<Integer> chapterIDs, String difficultyLevel) throws SQLException, QuestionsNotFoundException {
        String sql = "";
        
        if(paperType == this.UT){
            sql = "SELECT question, marks FROM questions WHERE difficulty_level IN(?, 'NORMAL') AND marks IN(2,4) AND chapter_id IN(";
        }else if(paperType == this.SEMESTER){ 
            sql = "SELECT question, marks FROM questions WHERE difficulty_level IN(?, 'NORMAL') AND chapter_id IN(";
        }
        
        for(int i: chapterIDs){
                sql+= "?,";
            }
            sql = sql.substring(0, sql.length()-1);
            sql+=")";
        System.out.println(sql);
        ps = conn.prepareStatement(sql);
        ps.setString(1, difficultyLevel);
        
        int j = 2;
        for(int i: chapterIDs){
            ps.setInt(j++, i);
        }
        ResultSet rs = ps.executeQuery();
        Map<String, Integer> questionPaper = new HashMap<>();
        while(rs.next()){
            questionPaper.put(rs.getString("question"), rs.getInt("marks"));
        }
        
        if(paperType == this.UT){
            this.checkQuestionsLengthAndGenerateUnitTest(questionPaper);
        }else if(paperType == this.SEMESTER){ 
            this.checkQuestionsLengthAndGenerateSemester(questionPaper);
        }
        
    }

    private void checkQuestionsLengthAndGenerateUnitTest(Map<String, Integer> questionPaper) throws QuestionsNotFoundException {
        if(questionPaper.size() < 10){
            throw new QuestionsNotFoundException("Questions Are Not Enough! Paper cannot be generated!");
        }
        List<Integer> marks = new ArrayList<>(questionPaper.values());
        
        int countOf2 = 0;
        int countOf4 = 0;
        for(int i: marks){
            if(i == 2){
                countOf2++;
            }
            if(i == 4){
                countOf4++;
            }
        }
        
        if(countOf2 < 6){
            throw new QuestionsNotFoundException("Two Marks Questions Are Not Enough! Paper cannot be generated!");
        }
        
        if(countOf4 < 4){
            throw new QuestionsNotFoundException("Four Mark Questions Are Not Enough! Paper cannot be generated!");
        }
        ub.randomTwoAndFourMarksQuestions(questionPaper);
    }

    private void checkQuestionsLengthAndGenerateSemester(Map<String, Integer> questionPaper) throws QuestionsNotFoundException {
        if(questionPaper.size() < 24){
            throw new QuestionsNotFoundException("Questions Are Not Enough! Paper cannot be generated!");
        }  
        
        sb.generateSemesterPaper(questionPaper);
    }

    public HashMap<String, Integer> getQuestionsForChapterId(int chapterId) throws SQLException{
        
        HashMap<String, Integer> questions;
        questions = new HashMap<>();
        String sql = "SELECT question, marks from questions WHERE chapter_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, chapterId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            questions.put(rs.getString("question"), rs.getInt("marks"));
        }       
        return questions;
    }

    public Question getQuestionByQuestionName(String question) throws SQLException {
        Question q = null;
        String sql = "SELECT * from questions WHERE question = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, question);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            q = new Question(rs.getString("question"), rs.getInt("marks"), rs.getString("difficulty_level"), rs.getInt("chapter_id"), rs.getInt("user_id"));
        }
        return q;
    }
    
    public int getQuestionIdByQuestionName(String question) throws SQLException {
        String sql = "SELECT id from questions WHERE question = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, question);
        ResultSet rs = ps.executeQuery();
        int questionId = 0;
        if(rs.next()){
            questionId = rs.getInt("id");
        }
        return questionId;
    }

    
    public void storeQuestion(Question q) throws SQLException {
        String sql = "INSERT INTO questions(chapter_id, question, marks, difficulty_level, user_id, created_at) values (?, ?, ?, ?, ?, ?)";
        
        ps = conn.prepareStatement(sql);

        ps.setInt(1, q.getChapterId());
        ps.setString(2, q.getQuestionName());
        ps.setInt(3, q.getMarks());
        ps.setString(4, q.getDifficultyLevel());
        ps.setInt(5, q.getTeacherId());
        ps.setString(6, q.getCreatedDate());
        
        ps.execute();
    }
    
    public void updateQuestion(Question q, int questionId) throws SQLException{
        String sql = "UPDATE questions set question = ?, marks = ?, difficulty_level = ?, updated_at = ?  WHERE id = ?";
        
        ps = conn.prepareStatement(sql);

        ps.setString(1, q.getQuestionName());
        ps.setInt(2, q.getMarks());
        ps.setString(3, q.getDifficultyLevel());
        ps.setString(4, q.getUpdatedDate());
        ps.setInt(5, questionId);
        
        System.out.println(ps.toString());
        ps.execute();
    } 
    
    public void removeQuestion(int questionId) throws SQLException {
        String sql = "DELETE FROM questions WHERE id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, questionId);
        ps.execute();        
    }

}
