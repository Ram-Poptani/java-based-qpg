/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import custom.Teacher;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rahul
 */
public class TeacherDB {
    private Connection conn;
    private PreparedStatement ps;
    
    
    public TeacherDB(){
        conn = MySQLConnect.getConnection();
        ps = null;
    }
    
    public void storeTeacher(Teacher t) throws SQLException{
        String sql = "INSERT INTO teachers(name, email, password, contact_no, created_at) values (?, ?, ?, ?, ?)";
       
        ps = conn.prepareStatement(sql);

        ps.setString(1, t.getName());
        ps.setString(2, t.getEmail());
        ps.setString(3, t.getPassword());
        ps.setString(4, t.getContact());
        ps.setString(5, t.getCreatedDate());

        ps.execute();
    }
    
    public ResultSet login(String email, String password) throws SQLException{
        String sql = "SELECT * FROM teachers WHERE email = ? AND password = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, email);
        ps.setString(2, password);

        ResultSet rs = ps.executeQuery();
        return rs;
    };
    
    public String getTeacherNameById(int teacherId) throws SQLException{
        String sql = "SELECT name FROM teachers WHERE ID = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, teacherId);
        ResultSet rs = ps.executeQuery();
        String teacherName = null;
        if(rs.next()){
            teacherName = rs.getString("name");
        }
        return teacherName;
    }
    
    public int getTeacherIdByTeacherName(String teacher) throws SQLException{
        System.out.println(teacher);
        String sql = "SELECT id FROM teachers WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, teacher);
        ResultSet rs = ps.executeQuery();
        int teacherId = 0;
        if(rs.next()){
            teacherId = rs.getInt("id");
        }
        return teacherId;
    }

    public ArrayList<String> getCoursesForTeacher(int teacherId) throws SQLException {
        ArrayList<String> courses = new ArrayList<>();
        String sql = "SELECT DISTINCT courses.name FROM courses JOIN course_subject ON courses.id = course_subject.course_id JOIN subjects ON subjects.id = course_subject.subject_id WHERE subjects.user_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, teacherId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            courses.add(rs.getString("name"));
            System.out.println(rs.getString("name"));
        }       
        return courses;
    }
    
    public ArrayList<String> getAllCourses() throws SQLException {
        ArrayList<String> courses = new ArrayList<>();
        String sql = "SELECT name FROM courses";
        ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            courses.add(rs.getString("name"));
        }       
        return courses;
    }

    public ArrayList<String> getSubjectsForCourse(String course, int teacherId) throws SQLException {
        int courseId = getCourseIdByCourseName(course);
        ArrayList<String> subjects = new ArrayList<>();
        String sql = "SELECT DISTINCT name from subjects JOIN course_subject ON course_subject.subject_id = subjects.id WHERE course_subject.course_id = ? AND subjects.user_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, courseId);
        ps.setInt(2, teacherId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            subjects.add(rs.getString("name"));
            System.out.println(rs.getString("name"));
        }       
        return subjects;
    }
    
    public ArrayList<String> getChaptersForSubject(String subject) throws SQLException {
        ArrayList<String> chapters = new ArrayList<>();
        if(subject != ""){
            return getChaptersForSubjectID(this.getSubjectIdBySubjectName(subject));
        }
        return chapters;
    }
    
    public ArrayList<Integer> getChapterIDsForSubject(String subject) throws SQLException {
        ArrayList<Integer> chapters = new ArrayList<>();
        if(subject != ""){
            return getChapterIDsForSubjectID(this.getSubjectIdBySubjectName(subject));
        }
        return chapters;
    }
    
    public ArrayList<String> getChaptersForSubjectID(int subjectId) throws SQLException {
        ArrayList<String> chapters = new ArrayList<>();
        String sql = "SELECT name from chapters WHERE subject_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, subjectId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            chapters.add(rs.getString("name"));
            System.out.println(rs.getString("name"));
        }       
        return chapters;
    }

    public ArrayList<Integer> getChapterIDsForSubjectID(int subjectId) throws SQLException {
        ArrayList<Integer> chapters = new ArrayList<>();
        String sql = "SELECT id from chapters WHERE subject_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, subjectId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            chapters.add(rs.getInt("id"));
        }       
        return chapters;
    }

    private int getCourseIdByCourseName(String course) throws SQLException {
        String sql = "SELECT id from courses WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, course);
        ResultSet rs = ps.executeQuery();
        int courseId = 0;
        if(rs.next()){
            courseId = rs.getInt("id");
        }
        return courseId;
    }
    
    public int getSubjectIdBySubjectName(String subject) throws SQLException {
        String sql = "SELECT id from subjects WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, subject);
        ResultSet rs = ps.executeQuery();
        int subjectId = 0;
        if(rs.next()){
            subjectId = rs.getInt("id");
        }
        return subjectId;
    }

    public int getChapterIdByChapterName(String chapter) throws SQLException {
        String sql = "SELECT id from chapters WHERE name = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, chapter);
        ResultSet rs = ps.executeQuery();
        int chapterId = 0;
        if(rs.next()){
            chapterId = rs.getInt("id");
        }
        return chapterId;
    }

    public void removeChapter(int chapterId) throws SQLException {
        this.removeQuestionsForChapter(chapterId);
        String sql = "DELETE FROM chapters WHERE id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, chapterId);
        ps.execute();        
    }
    
    private void removeQuestionsForChapter(int chapterId) throws SQLException{
        String sql = "DELETE FROM questions WHERE chapter_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, chapterId);
        ps.execute();        
    }    

    public ArrayList<Integer> getSemestersForTeacher(int teacherId) throws SQLException {
        ArrayList<Integer> semesters = new ArrayList<>();
        String sql = "SELECT DISTINCT semester FROM subjects WHERE user_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, teacherId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            semesters.add(rs.getInt("semester"));
        }
        return semesters;
    }

    public ArrayList<String> getSubjectsForTeacherAsPerSem(int sem, int teacherId) throws SQLException {
        ArrayList<String> subjects = new ArrayList<>();
        String sql = "SELECT name FROM subjects WHERE semester = ? AND user_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, sem);
        ps.setInt(2, teacherId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            subjects.add(rs.getString("name"));
        }       
        return subjects;
    }
    
    public ArrayList<String> getSubjectsForTeacher(int teacherId) throws SQLException {
        ArrayList<String> subjects = new ArrayList<>();
        String sql = "SELECT name FROM subjects WHERE user_id = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, teacherId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            subjects.add(rs.getString("name"));
        }       
        return subjects;
    }

    ArrayList<Integer> getChapterIDsForChapters(ArrayList chaptersList) throws SQLException {
        ArrayList<Integer> chapterIDs = new ArrayList<>();
        for(Object chapter: chaptersList){
            String sql = "SELECT id from chapters WHERE name = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, chapter.toString());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                chapterIDs.add(rs.getInt("id"));
            }
        }
        return chapterIDs;
    }
    
    public ArrayList<String> getAllTeachers() throws SQLException {
        ArrayList<String> teachers = new ArrayList<>();
        String sql = "SELECT name FROM teachers WHERE isAdmin = 0";
        ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            teachers.add(rs.getString("name"));
        }       
        return teachers;
    }
    
}
