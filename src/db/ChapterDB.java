/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import static UI.AddChapter.teacherId;
import UI.TeacherPanel;
import custom.Chapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author rahul
 */
public class ChapterDB {
    private Connection conn;
    private PreparedStatement ps;
    
    
    public ChapterDB(){
        conn = MySQLConnect.getConnection();
        ps = null;
    }
    
    public void storeChapter(Chapter c) throws SQLException{
        String sql = "INSERT INTO chapters(subject_id, name, created_at) values (?, ?, ?)";
        
        ps = conn.prepareStatement(sql);

        ps.setInt(1, c.getSubjectId());
        ps.setString(2, c.getName());
        ps.setString(3, c.getCreatedDate());
        
        ps.execute();
    } 
    
    public void updateChapter(Chapter c, int chapterId) throws SQLException{
        String sql = "UPDATE chapters set name = ? WHERE id = ?";
        
        ps = conn.prepareStatement(sql);

        ps.setString(1, c.getName());
        ps.setInt(2, chapterId);
        
        ps.execute();
    } 

    public String getOldValue(int chapterId) throws SQLException {
        String sql = "SELECT name FROM chapters WHERE id = ?";
        
        ps = conn.prepareStatement(sql);

        ps.setInt(1, chapterId);
       
        ResultSet rs = ps.executeQuery();
        String name = null;
        if(rs.next()){
            name = rs.getString("name");
        }
        return name;
    }
    
    public String getChapterNameByChapterId(int chapterId) throws SQLException {
        String sql = "SELECT name FROM chapters WHERE id = ?";
        
        ps = conn.prepareStatement(sql);
        ps.setInt(1, chapterId);
       
        ResultSet rs = ps.executeQuery();
        String name = null;
        if(rs.next()){
            name = rs.getString("name");
        }
        return name;
    }
}